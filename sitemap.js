"use strict";
exports.__esModule = true;
var books_json = require("./src/books.json");
var fs = require("fs");

const base = "https://www.heribertotejo.com";
const date = "2019-08-22";

function record(url, priority, img) {
    const imgPart = !!img ? `
        <image:image>
            <image:loc>${base}${img}</image:loc>
        </image:image>\n`: '';
    return `    <url>
        <loc>${base}/${url}</loc>
        <lastmod>${date}</lastmod>
        <changefreq>monthly</changefreq>
        <priority>${priority.toFixed(2)}</priority>${imgPart}
    </url>\n`;
}

const header = `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" 
  xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">\n`;
const footer = `</urlset>\n`;

const children = books_json
    .sort((a, b) => b.year - a.year) // sorting
    .map((b, i) => record(`libros/${b.code}`, 0.8 - (0.5 / books_json.length * (i + 1)), `/books/cover/${b.code}.jpg`))
    .join('\n');

const data = header + record("", 1.0) + record("libros", 0.9) + record("biografia", 0.8, '/images/heriberto-tejo.jpg') + children + footer;

fs.writeFileSync("./public/sitemap.xml", data);

console.log('Sitemap written!')

