import * as React from "react";
import { Link } from "react-router-dom";
export interface IHeaderState {
  open: boolean;
}

export interface IHeaderProps {

}

export default class NavMenu extends React.Component<IHeaderProps, IHeaderState> {

  constructor(props: IHeaderProps) {
    super(props);
    this.state = {
      open: false
    };
  }

  public render() {
    return (
      <nav className="nav">
          
          <Link to="/" className="nav__title">Heriberto Tejo</Link>
          <Link to={`/libros`} className="nav__item">Libros</Link>
          <Link to={`/biografia`} className="nav__item">Biografía</Link>

          <svg className={this.state.open ? "nav__handler nav__handler--open" : "nav__handler nav__handler--close"} viewBox="0 0 18 12" >
            <g id="m1"><rect x="0" y="0" width="18" height="2" /></g>
            <g id="m2"><rect x="0" y="5" width="18" height="2" /></g>
            <g id="m3"><rect x="0" y="10" width="18" height="2" /></g>
          </svg>
      </nav>
    );
  }
}
