import * as React from 'react';
import { Link } from "react-router-dom";
import { IBook } from '../model/book.model';
import * as StackBlur from 'stackblur-canvas';

export interface IBookItemProps {
    book: IBook
}

export class BookItem extends React.Component<IBookItemProps> {

    private canvasRef: React.RefObject<HTMLCanvasElement>;

    constructor(props: IBookItemProps) {
        super(props);
        this.canvasRef = React.createRef<HTMLCanvasElement>();
    }
    componentDidMount() {
        this.updateCanvas();
    }

    hideCanvas() {
        if (!this.canvasRef.current) return;
        this.canvasRef.current.setAttribute("class", "loading-hide");
    }
    updateCanvas() {
        if (!this.props.book) return;
        //const canvas = <HTMLCanvasElement>document.getElementById('canvasId');
        if (!this.canvasRef.current) return;
        const ratio = this.props.book.imageHeight / this.props.book.imageWidth;
        this.canvasRef.current.height = Math.min(230, this.canvasRef.current.width * ratio);
        this.canvasRef.current.width = this.canvasRef.current.height / ratio;
        const ctx = this.canvasRef.current.getContext('2d');
        if (!ctx) return;

        const colors = this.props.book.imageColors;
        // ["f30046","f00043","f00342","d63334","898c4d","f4104f","f11d54","e83560","d54a64","716236","ef1447","ef2753","c45f72","c49d7d","802c0a","ef1445","e84e6b","d2aca2","a26949","ad5315","e40038","e20439","dc3858","a4420e","df721f"];


        const p = 5;
        const w = Math.floor(ctx.canvas.width / p);
        const h = Math.floor(ctx.canvas.height / p);
        for (let i = 0; i < (p * p); i++) {
            console.log('.');
            ctx.fillStyle = '#' + colors[i] + 'FF'; //`rgb(${50*Math.floor(i / p) },25,84)`;
            ctx.fillRect(w * Math.floor(i / p), h * (i % p), w, h);
        }
        StackBlur.canvasRGB(this.canvasRef.current, 0, 0, ctx.canvas.width, ctx.canvas.height, 50);
        //this.canvasRef.current.style.webkitFilter = `blur(${w/3}px)`;

    }
    public render() {
        return (
            <div className="book-item">
                <Link to={`/libros/${this.props.book.code}`}>
                    <img src={`books/ficha/${this.props.book.code}_ficha.jpg`} onLoad={() => this.hideCanvas()} alt="" />
                    <canvas ref={this.canvasRef} width={160} height={230} />
                    <figcaption>{this.props.book.title} ({this.props.book.year})</figcaption>
                </Link>
            </div>
        );
    }
}

export default BookItem;