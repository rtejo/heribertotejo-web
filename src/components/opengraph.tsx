import * as React from 'react';
import { IOpenGraph } from '../model/book.model';
import Helmet from 'react-helmet';

export default class OpenGraph extends React.Component<IOpenGraph> {


    public render() {
        const site = "Heriberto Tejo";
        return (
            <Helmet>
                <title>{this.props.book ? `${site} - ${this.props.book.title}` : `${this.props.title}`}</title>
                <meta property="description" content={this.props.description} />
                <meta property="og:title" content={this.props.title} />
                <meta property="og:description" content={this.props.book ? this.props.book.back : this.props.description} />
                <meta property="og:locale" content="es_PE" />
                <meta property="og:site_name" content={site} />
                <meta property="og:url" content={window.location.href} />
                <meta property="og:image" content={window.location.origin + this.props.image} />
                <meta property="og:type" content={this.props.book ? "books.book" : "books.author:"} />

                {this.props.book && (<meta property="og:title" content={this.props.book.title} />)}
                {this.props.book && (<meta property="books:isbn" content={this.props.book.isb} />)}
                {this.props.book && (<meta property="books:author" content={window.location.origin} />)}
                    
                

            </Helmet>
        );
    }
}