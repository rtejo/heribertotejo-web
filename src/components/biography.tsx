import * as React from 'react';

export interface IBiographyProps {
}

//TODO: arreglar links (usando <Link> y books.json )
export default class Biography extends React.Component<IBiographyProps> {
    public render() {
        return (
            <div className="biography">
                <p>Nací en Villacidaler, un pequeño pueblo castellano de España, en diciembre de 1951. El segundo de cuatro hermanos.</p>
                <img src="/images/bio-1.jpg" alt="Foto de Heriberto Tejo" />
                <p>De pequeño, recuerdo el corral inmenso y soleado de la casa, lleno de gallinas y conejos, donde jugaba con mis hermanos y mis primos horas y horas. Recuerdo que entre un montón de musgosas tejas cultivaba con asombro hierbabuena y perejil. De la escuela, donde aprendí a leer y escribir en compañía de chicos de 7 y 14 años, recuerdo que me gustaba mucho dibujar y pintar. Mi maestro don Julio, me felicitaba en público por el colorido de mis dibujos. Eso sí, tanto en la escuela como en casa, recuerdo que, como era zurdo, me obligaban a escribir y a comer con la mano derecha, cosa que logré con éxito. Con mis amigos, recuerdo los recreos en la plaza mayor, los juegos nocturnos alrededor de la iglesia y los paseos de las increíbles tardes correteando por el campo y la orilla del río en busca de nidos, pájaros, uvas y manzanas. ¡Qué tardes!</p>
                <img src="/images/bio-2.jpg" alt="Foto de Heriberto Tejo" />
                <p>Mis padres, Francisco y Celestina, trabajaban duro en las labores del campo con sol y lluvia para que no faltara nunca un pan en la mesa. La secundaria la hice en la ciudad de Valladolid en un colegio de los Hermanos Maristas. Fueron años de juego y amistad y, cómo no, de acercamiento a la lectura: Robinson Crusoe, La Ilíada, Perdidos en el bosque y muchos cuentos y poemas de la colección El tesoro de la Juventud. Recuerdo que mi primera experiencia de escritura fue hacer un pequeño diario en una libretita durante un mes y medio. Creo que fue a raíz de escuchar algunas páginas del diario de Ana Frank. Luego vinieron mis primeros poemas. La música y la poesía a mis 16 años eran mis amigas inseparables.</p>
                <img src="/images/bio-3.jpg" alt="Foto de Heriberto Tejo" />
                <p>A los 18 años llegué a Lima (Perú) donde me impactó tremendamente el mar y las gaviotas. En ese tiempo estudié Educación (5 años) y leí mucha poesía, cautivándome Lorca, Antonio Machado, Alberti, Juan Ramón Jiménez, Neruda, Eguren, Vallejo, Javier Heraud. Justamente los primeros libros que publiqué fueron de poesía. A mis 24 años tenía escritos dos poemarios fruto de un corazón enamorado, pero el primer libro que publiqué fue Canto de gorriones en 1986, poemario con el que gané el Primer Premio Nacional de Literatura Infantil. Para ese tiempo, estaba ya casado con Rosa Silva y tenía mis tres hijos, Erika, Ricardo y Juan Carlos, con quienes jugaba y disfrutaba contándoles cuentos.</p>
                <img src="/images/bio-4.jpg" alt="Foto de Heriberto Tejo" />
                <p>Luego, el fervor por la poesía infantil se incrementó y publiqué los libros Magia de primavera (1989) y Camino del arco iris (1991), ambos galardonados nuevamente con el Primer Premio Nacional de Literatura Infantil causando un suceso único. Entonces sí, con la alegría de los premios obtenidos, me animé a publicar en edición de autor mis primeros poemas juveniles: Contigo siempre /Algo vital (1992), una exaltación gozosa del amor y la vida. La llega a miles de lectores, sin embargo, ocurrió con la publicación  del libro Hola caracol (Alfaguara, 1997), un conjunto de poemas breves llenos de humor y musicalidad, y a la serie de Mi amigo el glumpo (Alfaguara, 1998), ese personaje de grandes orejas de lechuga y corazón transparente que ronda por la casa y fascina a tantos niños.</p>
                <img src="/images/bio-5.jpg" alt="Foto de Heriberto Tejo" />
                <p>Tengo publicados más de treinta libros y confieso que me siento muy bien escribiendo para los niños, sean estos pequeños o mayores. Al hacerlo sueño con sus mundos, sus fantasías, sus juegos, sus esperanzas. He recibido varios premios y reconocimientos nacionales e internacionales, pero el mejor premio de todos son los lectores, niños y niñas con los que no me canso de conversar cuando voy a los colegios, con los que me divierto y río, con los que me enriquezco siempre. Ellos siempre me piden que continúe escribiendo, yo también le digo que no dejen de soñar, que sigan leyendo y escribiendo desde su mundo. La lectura y escritura siempre será un excelente medio de enriquecimiento personal.</p>
                <img src="/images/bio-7.jpg" alt="Foto de Heriberto Tejo" />
                <img src="/images/bio-6.jpg" alt="Foto de Heriberto Tejo" />
            </div>



        );
    }
}