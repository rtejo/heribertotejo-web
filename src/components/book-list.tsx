// tslint:disable: jsx-no-multiline-js
import React from 'react';
import BookItem from './book-item';
import { Context, ContextState } from '../app-context';
import { IBook } from '../model/book.model';
import InputRange from 'react-input-range'
import CheckboxGroup from 'react-checkbox-group'

export interface IBookListProps {
}
export interface IBookListState {
  books: IBook[]
  filter: { active: boolean, year: { min: number, max: number }, publisher: string[] }
  filterElements: { year: { min: number, max: number }, publisher: string[] }
}

export default class BookList extends React.Component<IBookListProps, IBookListState> {

  static contextType = Context; // RECT Things!

  private get books(): IBook[] { return (this.context as ContextState).books; };

  constructor(props: IBookListProps, context: ContextState) {
    super(props, context);

    const filterElements = { publisher: new Set<string>(), year: new Set<number>() };

    for (const { publisher, year } of this.books) {
      filterElements.publisher.add(publisher);
      filterElements.year.add(year);
    }

    const _years = Array.from(filterElements.year).sort();
    const publisher = Array.from(filterElements.publisher).sort().filter(a => a.trim().length > 0);
    const year = { min: _years[0], max: _years[_years.length - 1] }

    //---
    this.state = this.applyFilter({
      books: [],
      filter: { active: false, year, publisher },
      filterElements: { year, publisher }
    });

    // RECT Things!
    this.handleToggleFilter = this.handleToggleFilter.bind(this);
    this.handleFilterYear = this.handleFilterYear.bind(this);
    this.handleFilterPublisher = this.handleFilterPublisher.bind(this);
  }

  private handleToggleFilter(e: React.MouseEvent) {
    this.setState((current) => ({ ...current, filter: { ...current.filter, active: !current.filter.active } }));
  }
  private handleFilterYear(year: any) {
    this.setState((current) => this.applyFilter({ ...current, filter: { ...current.filter, year } }));
  }
  private handleFilterPublisher(publisher: string[]) {
    this.setState((current) => this.applyFilter({ ...current, filter: { ...current.filter, publisher } }));
  }

  private applyFilter(newFilter: IBookListState): IBookListState {
    const filteredBooks = this.books.filter(b => (
      (b.year >= newFilter.filter.year.min && b.year <= newFilter.filter.year.max) &&
      (b.publisher === '' || newFilter.filter.publisher.includes(b.publisher))
    )).sort((a, b) => b.year - a.year);
    return { ...newFilter, books: filteredBooks };
  }


  public render() {

    return (
      <>
        <div className="book-list">
          {}
          <div className={this.state.filter.active ? 'book-filter filter-show' : 'book-filter filter-hide'}  >
            <span>{this.filterText()}</span>
            <svg onClick={this.handleToggleFilter} xmlns="http://www.w3.org/2000/svg" fillRule="evenodd" strokeLinejoin="round" strokeMiterlimit="2" clipRule="evenodd" viewBox="0 0 66 66"><path fill="none" d="M0 0h66v66H0z" /><path fillRule="nonzero" d="M33.244.006c15.07.144 29.2 11.68 32.15 26.554 4.08 20.566-14.99 43.232-38.41 38.892-13.42-2.488-24.63-14.116-26.61-27.63-2.68-18.306 12.19-37.37 32.01-37.81.29-.005.57-.007.86-.006zm-.38 7.167c-15.4.149-28.96 15.85-24.89 32.14 2.77 11.105 13.72 19.596 25.22 19.524 14.28-.09 27.27-13.46 25.5-28.796-1.44-12.447-12.77-22.91-25.83-22.868zm.16 33.843v2.003h14.02c1.1 0 2 .9 2 2.002 0 1.102-.9 2.003-2 2.003h-14.02v2.002h-4v-2.002h-10.02c-1.1 0-2-.901-2-2.003 0-1.101.9-2.002 2-2.002h10.02v-2.003h4zM41.034 29v2.003h6.01c1.1 0 2 .9 2 2.003 0 1.1-.9 2.002-2 2.002h-6.01v2.003h-4v-2.003h-18.03c-1.1 0-2-.901-2-2.002 0-1.102.9-2.003 2-2.003h18.03V29h4zm-12.01-12.015v2.002h18.02c1.1 0 2 .9 2 2.003 0 1.1-.9 2.002-2 2.002h-18.02v2.003h-4.01v-2.003h-6.01c-1.1 0-2-.901-2-2.002 0-1.102.9-2.003 2-2.003h6.01v-2.002h4.01z" /></svg>
          </div>

          <div className="filter-panel">
            <div className="filter-year">
              <label>Año</label>
              <InputRange
                maxValue={this.state.filterElements.year.max}
                minValue={this.state.filterElements.year.min}
                value={this.state.filter.year}
                onChange={this.handleFilterYear}
              />
            </div>
            <div className="filter-publisher">
              <label>Editorial</label>
              <CheckboxGroup name="fruits" value={this.state.filter.publisher} onChange={this.handleFilterPublisher}>
                {(Checkbox) => (
                  <div className="checkbox-group">
                    {this.state.filterElements.publisher.map((p) =>
                      (<label className={this.state.filter.publisher.indexOf(p) !== -1 ? "checked" : ""} key={p} ><Checkbox value={p} />{p}</label>)
                    )}
                  </div>
                )}
              </CheckboxGroup>
            </div>

          </div>

          <div className="book-items">
            {this.state.books.map((book) => (<BookItem key={book.code} book={book} />))}
          </div>
        </div>
      </>
    )
  }

  private filterText(): React.ReactNode {
    const f = this.state.books.length, t = this.books.length;
    return (f < t) ? `Mostrando ${f} libro${f === 1 ? '' : 's'} de ${t}` : `Mostrando todos, los ${t} libros`;
  }
}
