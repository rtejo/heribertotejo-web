import * as React from "react";


export default class Social extends React.Component {

  public render() {
    return (
      <div className="social">
        <a className="footer-fb ht-fb" href="https://www.facebook.com/heribertotejo"  target="_blank"  rel="noopener noreferrer" >facebook.com/heribertotejo</a>
        <a className="footer-email ht-mail" href="mailto://correo@heribertotejo.com"  target="_blank"  rel="noopener noreferrer">correo@heribertotejo.com</a>
      </div>
    );
  }
}
