import React from "react";
import { RouteComponentProps, withRouter } from "react-router";


// HACK: Horrible solution to scroll problem on React-Router
class ScrollToTop extends React.Component<RouteComponentProps> {
  componentDidUpdate(prevProps: Readonly<RouteComponentProps>) {

    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0)
    }
  }

  render() {
    return this.props.children
  }
}

export default withRouter(ScrollToTop)