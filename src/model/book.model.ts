export interface IBook {
    code: string,
    title: string,
    publisher: string,
    collection: string,
    year: number,
    pages: number,
    isb: string,
    illustrator: string,
    category: string,
    age: string, // TODO: change to number.. CSV-JSON (put null)
    back: string,

    imageWidth: number,
    imageHeight: number,
    imageColors: string[]
}

export interface IBookItem {
    code: string,
    title: string,
    year: number,
}

export interface IFilter {
    year: number,
}

export interface IOpenGraph {
 title: string;
 image: string;
 description?: string;
 book? : IBook;

}