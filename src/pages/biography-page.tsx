import * as React from 'react';
import Biography from '../components/biography';
import OpenGraph from '../components/opengraph';
import NavMenu from '../components/nav-menu';


export default class BiographyPage extends React.Component {
    public render() {
        const description = "Biografía de Heriberto Tejo.";
        return (
            <>
                <OpenGraph title="Heriberto Tejo" image={`/images/heriberto-tejo.jpg`} description={description} />
                <NavMenu />
                <h2 className="section-title" title="Biografía de Heriberto Tejo">Biografía</h2>
                <Biography />
            </>

        );
    }
}