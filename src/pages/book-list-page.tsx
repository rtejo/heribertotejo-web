import * as React from 'react';
import OpenGraph from '../components/opengraph';
import NavMenu from '../components/nav-menu';
import BookList from '../components/book-list';

export default class BookListPage extends React.Component {
    public render() {
        const description = "Libros de Heriberto Tejo.";
        return (
            <>
                <OpenGraph title="Heriberto Tejo" image={`/images/heriberto-tejo.jpg`} description={description} />
                <NavMenu />
                <h2 className="section-title">Libros</h2>
                <BookList />
            </>

        );
    }
}