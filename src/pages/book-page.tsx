import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { IBook } from '../model/book.model';
import { ContextConsumer } from '../app-context';
import OpenGraph from '../components/opengraph';
import NavMenu from '../components/nav-menu';
import * as StackBlur from 'stackblur-canvas';

export default class BookPage extends React.Component<IBookProps> {

    private canvasRef: React.RefObject<HTMLCanvasElement>;

    constructor(props: IBookProps) {
        super(props);
        this.canvasRef = React.createRef<HTMLCanvasElement>();
    }
    componentDidMount() {
        this.updateCanvas();
    }
    hideCanvas() {
        if (!this.canvasRef.current) return;
        this.canvasRef.current.setAttribute("class", "loading-hide");
    }
    updateCanvas() {
        if (!this.book) return;
        //const canvas = <HTMLCanvasElement>document.getElementById('canvasId');
        if (!this.canvasRef.current) return;
        
        if (!this.canvasRef.current) return;
        const ratio = this.book.imageHeight / this.book.imageWidth;
        this.canvasRef.current.height = this.canvasRef.current.width * ratio;
        const ctx = this.canvasRef.current.getContext('2d');
        if (!ctx) return;

        const colors = this.book.imageColors;
        const p = 5;
        const w = Math.floor(ctx.canvas.width / p);
        const h = Math.floor(ctx.canvas.height / p);
        for (let i = 0; i < (p * p); i++) {
            console.log('.');
            ctx.fillStyle = '#' + colors[i] + 'FF'; //`rgb(${50*Math.floor(i / p) },25,84)`;
            ctx.fillRect(w * Math.floor(i / p), h * (i % p), w, h);
        }
        StackBlur.canvasRGB(this.canvasRef.current, 0, 0, ctx.canvas.width, ctx.canvas.height, 50);
        //this.canvasRef.current.style.webkitFilter = `blur(${w/3}px)`;

    }
    private book: IBook | null = null;
    private makeBook(books: { [code: string]: IBook }) {

        let book = books[this.props.match.params.book];
        this.book = book;
        const description = `Ficha del libro "${book.title}" de Heriberto Tejo.`;
        return (
            <>
                <NavMenu />
                <OpenGraph title={book.title} image={`/books/cover/${book.code}.jpg`} description={description} book={book} />

                <main className="book-view">
                    <h1>{book.title}</h1>
                    <div className="book-view__cover">
                        <canvas ref={this.canvasRef} width={340} height={350} />
                        <img src={`/books/cover/${book.code}.jpg`} onLoad={() => this.hideCanvas()} alt={book.title} />
                    </div>
                    <div className="book-view__backtext">{book.back}</div>
                    <div className="book-view__info">
                        <h2>Ficha</h2>
                        <div><span>Titulo:</span> <span>{book.title}</span></div>
                        {book.illustrator && (<div><span>Ilustraciones de:</span> <span>{book.illustrator}</span></div>)}
                        <div><span>Año de publicación:</span> <span>{book.year}</span></div>
                        <div><span>Editorial:</span> <span>{book.publisher}</span></div>
                        <div><span>Páginas:</span> <span>{book.pages}</span></div>
                        <div><span>ISBN:</span> <span>{book.isb}</span></div>

                    </div>
                </main>
            </>
        );
    }

    public render() {
        return (
            <ContextConsumer>
                {(context:any) => this.makeBook(context.booksHash)}
            </ContextConsumer >
        )
    }
}

export interface IBookProps
    extends RouteComponentProps<{ book: string }> {
}


