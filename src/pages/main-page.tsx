import * as React from 'react';
import BookList from '../components/book-list';
import Biography from '../components/biography';
import Social from '../components/social';
import OpenGraph from '../components/opengraph';
import { Link } from 'react-router-dom';

export interface IMainProps {
}

export default class MainPage extends React.Component<IMainProps> {
  public render() {
    const description = "Sitio web oficial de Heriberto Tejo. Educador, escritor y poeta para niños. Bibliografia y biografía.";

    return (
      <main>

        <OpenGraph title="Heriberto Tejo" image={`/images/heriberto-tejo.jpg`} description={description} />

        <header>
          <h1>Heriberto Tejo</h1>
          <h2>Educador, poeta y narrador para niños</h2>
        </header>

        <div className="profile" />

        <div className="quote">
          <p>En un mundo tan pequeño,<br />necesitamos más unicornios<br />y menos guerreros</p>
        </div>

        <Social />
        <section className="books">
          <h2 className="section-title">
            <Link to={`/libros`}>Libros</Link>
          </h2>
          <BookList />
        </section>

        <section className="bio">
          <h2 className="section-title" title="Biografía de Heriberto Tejo">
            <Link to={`/biografia`}>Biografía</Link>
          </h2>
          <Biography />
        </section>

      </main>
    );
  }
}
