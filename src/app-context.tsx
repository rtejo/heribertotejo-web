import React from 'react';
import { IBook } from './model/book.model';
import books from './books.json';

export type ContextState = {
  books: IBook[]  // from DB
  booksHash: { [code: string]: IBook }  // from DB
};

export const Context = React.createContext<ContextState>({ books, booksHash: {} });

export class ContextProvider extends React.Component {
  render() {

    const booksHash: { [key: string]: IBook } = {};
    books.forEach(b => { booksHash[b.code] = b; });
    const newState: ContextState = {
      books,
      booksHash
    };

    return (
      <Context.Provider value={newState}>
        {this.props.children}
      </Context.Provider >
    );
  }
};

export const ContextConsumer = Context.Consumer;

//const [[, ...allBooks]] = Object.entries<IBook>(booksHash);
