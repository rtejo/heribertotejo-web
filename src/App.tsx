import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./App.scss";
import { ContextProvider } from "./app-context";
import ScrollToTop from './components/scroll-to-top';
import MainPage from "./pages/main-page";
import BookPage from "./pages/book-page";
import BookListPage from "./pages/book-list-page";
import BiographyPage from "./pages/biography-page";


export default class App extends React.Component {

  public render() {
    // this.setState((state, props) => ({
    //   counter: state.counter + props.increment
    // }));

    /* <img src={logo} className="App-logo" alt="logo" /> */
    //this.state
    return (
      <BrowserRouter>
        <ScrollToTop>
          <ContextProvider>
            <div className="content">
              <Switch>
                <Route exact path="/" component={MainPage} />
                <Route exact path='/libros' component={BookListPage} />
                <Route path="/libros/:book" component={BookPage} />
                <Route path="/biografia" component={BiographyPage} />
              </Switch>
            </div>
          </ContextProvider>
        </ScrollToTop>
      </BrowserRouter>
    );
  }
}

