// compile with:    tsc .\src\blur.ts --downlevelIteration
// execute:   node .\src\blur.js .\public\books\la-casita-bonita.jpg
import * as Jimp from 'jimp';

const pixels = parseInt(process.argv[2]);
const files = process.argv.slice(3);

const data = files.map(async (f) => {
    const key = f.substring(f.lastIndexOf("\\")+1, f.lastIndexOf("."));

    const img = await Jimp.read(f);
    const w = img.getWidth(), h = img.getHeight();

    const pp = img.resize(pixels, pixels);
    const colors = [...Array(pixels * pixels).keys()].map(i =>
        pp.getPixelColor(Math.floor(i / pixels), i % pixels).toString(16).substr(0, 6)
    );
    return { k: key, w, h, c: colors }
});

data.forEach(async r => {
    const s = await r;
    console.log(`${s.k}|${s.w}|${s.h}|${JSON.stringify(s.c)}`);
})

