import books from './src/books.json';
import fs from 'fs';

const base = "https://heribertotejo.com";
const date = "2019-08-22";
//"monthly" "1.00"

function record(url: string, priority: number): string {
    return `<url>
  <loc>${base}/${url}</loc>
  <lastmod>${date}</lastmod>
  <changefreq>monthly</changefreq>
  <priority>${priority.toFixed(2)}</priority>
</url>`;
}

const header = `<?xml version="1.0" encoding="UTF-8"?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">\n`;
const footer = `</urlset>\n`;
const children = books.map((b, i) => record(b.code, 0.8 - (0.5 / books.length * (i + 1)))).join('\n');

const data = header + record("libros", 0.9) + record("biografia", 0.8) + children + footer;

fs.writeFileSync("./public/sitemap2.xml", data);

console.log('Sitemap written!')

